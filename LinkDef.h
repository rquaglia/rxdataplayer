// Definitions for CINT and PYTHON interface of tuples
// For info have a look at
// https://root.cern.ch/selecting-dictionary-entries-linkdefh
// https://root.cern.ch/root/htmldoc/guides/users-guide/AddingaClass.html#other-useful-pragma-statements

#ifdef __ROOTCLING__

#pragma link off all class;
//#pragma link off all enum;
#pragma link C++ all function;
//#pragma link off all global;
#pragma link off all namespace;
//#pragma link off all struct;
//#pragma link off all typedef;
//#pragma link off all union;
#pragma link C++ nestedclasses;
#pragma link C++ nestedtypedef;

#pragma link C++ class MessageSvc+;
#pragma link C++ defined_in "MessageSvc.hpp";

// #pragma link C++ all function;
#pragma link C++ all functions; //necessary to have functions available!
#pragma link C++ class RXWeight+;
#pragma link C++ class RXSelection+;
#pragma link C++ class RXVarPlot+;
#pragma link C++ class RXDataPlayer+;
#pragma link C++ defined_in "RXDataPlayer.hpp";

#endif
