Simple shared libary to use ROOT multithreaded

Requires ROOT v6.14 at least (and BOOST)

Build it : 

```
mkdir build
cd build
cmake ..
make -j4
```

Inside ROOT
```
gSystem->Load(build/libRXDataPlayer)
//now ready to use RXDataPlayer
```