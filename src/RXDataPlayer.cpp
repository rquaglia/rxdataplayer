#ifndef RXDATAPLAYER_CPP
#define RXDATAPLAYER_CPP

#include "RXDataPlayer.hpp"
#include "boost/program_options.hpp"
#include "fmt_ostream.h"

#include "zipfor.h"
#include <algorithm>
#include <boost/progress.hpp>
#include <iostream>
#include <numeric>
#include <vector>
#include "TApplication.h"
#include "TTreeFormula.h"

#include "ROOT/RMakeUnique.hxx"
#include <ROOT/RDataFrame.hxx>
#include "TChain.h"
#include <utility>


RXDataPlayer::RXDataPlayer() {
    SetDebug(true);
    if (m_debug) MessageSvc::Debug("RXDataPlayer", (TString) "Default");
    MessageSvc::Line();
    MessageSvc::Info("RXDataPlayer", m_name);
    MessageSvc::Line();
}

RXDataPlayer::RXDataPlayer(TString _name) {
    SetDebug(true);
    if (m_debug) MessageSvc::Debug("RXDataPlayer", (TString) "TString");
    m_name = _name;
    MessageSvc::Line();
    MessageSvc::Info("RXDataPlayer", m_name);
    MessageSvc::Line();
}

void RXDataPlayer::Book1DHist(TString _expression, TString _label, int _nBins, double _min, double _max, TString _units, bool _isoBin, bool _useLogY) {
    MessageSvc::Info("Book1DHist", _expression);
    m_bookedVariable[m_nVars] = RXVarPlot(m_nVars, _expression, _label, _nBins, _min, _max, _units, _isoBin, _useLogY);
    m_nVars++;
    return;
}

void RXDataPlayer::Book1DHist(const RooRealVar & _var) {
    TString _expression = _var.GetName();
    TString _label      = _var.GetTitle();
    TString _units      = TString(_var.getUnit());
    int     _nBins      = _var.numBins();
    double  _min        = _var.getMin();
    double  _max        = _var.getMax();
    Book1DHist(_expression, _label, _nBins, _min, _max, _units, false, false);   // delegate bookkeping
    return;
}

void RXDataPlayer::BookSelection(TCut _selection, TString _label) {
    MessageSvc::Info("BookSelection", _label);
    m_bookedSelections[m_nSelections] = RXSelection(m_nSelections, _selection, _label);
    m_nSelections++;
    return;
}

void RXDataPlayer::BookWeight(TString _weight, TString _label) {
    MessageSvc::Info("BookWeight", _label);
    m_bookedWeights[m_nWeights] = RXWeight(m_nWeights, _weight, _label);
    m_nWeights++;
    return;
}

vector< tuple< RXSelection, RXWeight, RXVarPlot > > RXDataPlayer::BuildCombination() {
    // map< int, tuple<RXSelection, RXWeight, RXVarPlot  >  > RXDataPlayer::BuildCombination(){
    vector< tuple< RXSelection, RXWeight, RXVarPlot > > _final;
    if (GetNSelections() * GetNWeights() * GetNVariables() != 0) {
        // const map<int, RXSelection> & _RXSelections, const map<int, RXWeight>    & _RXWeights, const map<int, RXVarPlot> & _Variables){
        for (const auto & _vv : m_bookedVariable) {
            for (const auto & _ww : m_bookedWeights) {
                for (const auto & _cc : m_bookedSelections) { _final.push_back(make_tuple(_cc.second, _ww.second, _vv.second)); }
            }
        }
        return _final;
    } else {
        MessageSvc::Error("BuildCombination, ILLEGAL PROCEDURE: You must book at least 1 Selection, 1 Variable, 1 Weight", "EXIT_FAILURE");
    }
    return _final;
}

void RXDataPlayer::Process(TChain & _tuple) noexcept {
    MessageSvc::Line();
    MessageSvc::Info("RXDataPlayer", (TString) "Process ...");

    auto tStart = chrono::high_resolution_clock::now();

    MessageSvc::Info("Create", (TString) "RDataFrame");

    // cout<<"Cloning to have less entries " << endl;
    ROOT::EnableImplicitMT();   // Enable Multi threading
    ROOT::RDataFrame df(_tuple);
    // cout<<RED<< "define dummy"<< endl;
    auto firstDF  = df.Define("DUMMY", " 1>0 ?  1. :0.");
    using DFType  = decltype(firstDF);
    auto latestDF = make_unique< DFType >(firstDF);

    MessageSvc::Info("Book", (TString) "Variable(s)", to_string(m_bookedVariable.size()));
    for (auto & el : m_bookedVariable) {
        TString vv = fmt::format("Observable{0}", el.first);
        latestDF   = make_unique< DFType >(latestDF->Define(vv.Data(), el.second.Expression().Data()));
    }

    MessageSvc::Info("Book", (TString) "Selection(s)", to_string(m_bookedSelections.size()));
    for (auto & el : m_bookedSelections) {
        TString selection = TString(el.second.Expression()) + " ? 1.5 : -1.5";   // passed cut = > 0, failed cut < 0
        TString vv        = fmt::format("Selection{0}", el.first);
        latestDF          = make_unique< DFType >(latestDF->Define(vv.Data(), selection.Data()));
    }

    MessageSvc::Info("Book", (TString) "Weight(s)", to_string(m_bookedWeights.size()));
    for (auto & el : m_bookedWeights) {
        TString vv = fmt::format("Weight{0}", el.first);
        latestDF   = make_unique< DFType >(latestDF->Define(vv.Data(), el.second.Expression().Data()));
    }

    auto dummy  = latestDF->Take< double >("DUMMY");
    using DTYPE = decltype(dummy);
    map< RXSelection, DTYPE > _selectionValues;
    map< RXWeight, DTYPE >    _weightValues;
    map< RXVarPlot, DTYPE >   _variableValues;
    for (auto & el : m_bookedSelections) {
        TString vv                  = fmt::format("Selection{0}", el.first);
        _selectionValues[el.second] = latestDF->Take< double >(vv.Data());
    }
    for (auto & el : m_bookedWeights) {
        TString vv               = fmt::format("Weight{0}", el.first);
        _weightValues[el.second] = latestDF->Take< double >(vv.Data());
    }
    for (auto & el : m_bookedVariable) {
        TString vv                 = fmt::format("Observable{0}", el.first);
        _variableValues[el.second] = latestDF->Take< double >(vv.Data());
    }

    MessageSvc::Info("Processing", (TString) "");
    auto size = dummy->size();
    MessageSvc::Info("Processed", (TString) "Events =", to_string(size));
    // cout<< "LOADED ENTRIES = "<< size << endl;  //this trigger all parallel Take we booked

    MessageSvc::Info("Filling", (TString) "");   // this trigger all parallel Take we booked
    for (auto & _results : m_bookedVariable) {
        m_bookedVariableValues[_results.second].reserve(size);
        m_bookedVariableValues[_results.second] = move(*_variableValues[_results.second]);
    }
    for (auto & _results : m_bookedWeights) {
        m_bookedWeightResults[_results.second].reserve(size);
        m_bookedWeightResults[_results.second] = move(*_weightValues[_results.second]);
    }
    for (auto & _results : m_bookedSelections) { m_bookedSelectionResults[_results.second].reserve(size); }
    // cout<<"Populating by element "<< endl;
    for (int i = 0; i < size; ++i) {
        for (auto & _results : m_bookedSelections) {
            // _results.first.Print();
            // cout<<  _selectionValues[_results.second]->at(i) << endl;
            if (_selectionValues[_results.second]->at(i) > 0) {              // Linked to selection 1.5 : -1.5 OF selection line
                m_bookedSelectionResults[_results.second].push_back(true);   // Selection passed
            } else {
                m_bookedSelectionResults[_results.second].push_back(false);   // Selection reject this entry
            }
        }
    }

    auto tEnd      = chrono::high_resolution_clock::now();
    auto timeTotal = chrono::duration_cast< chrono::seconds >(tEnd - tStart).count();
    auto timeEvent = (double) size / (double) timeTotal;
    MessageSvc::Line();
    MessageSvc::Warning("RXDataPlayer", m_name, "took", fmt::format("{0} seconds [{1} evts/s] booking {2} DataInformation", timeTotal, timeEvent, GetNSelections() * GetNWeights() * GetNVariables()), "");
    MessageSvc::Line();
    return;
}

TH1D * RXDataPlayer::GetHistogram1D(int _idx_var, int _idx_cut, int _idx_weight, Int_t _color , TString _option ){
    if (_idx_var >= GetNVariables()) MessageSvc::Error("RXDataPlayer", "GetHistogram1D", to_string(_idx_var), to_string(GetNVariables()), "not existing Variable", "EXIT_FAILURE");
    if (_idx_cut >= GetNSelections()) MessageSvc::Error("RXDataPlayer", "GetHistogram1D", to_string(_idx_cut), to_string(GetNSelections()), "not existing Selection", "EXIT_FAILURE");
    if (_idx_weight >= GetNWeights()) MessageSvc::Error("RXDataPlayer", "GetHistogram1D", to_string(_idx_weight), to_string(GetNWeights()), "not existing Weight", "EXIT_FAILURE");

    auto _toFillValues     = m_bookedVariableValues[m_bookedVariable[_idx_var]];
    auto _toFillSelections = m_bookedSelectionResults[m_bookedSelections[_idx_cut]];
    auto _toFillWeights    = m_bookedWeightResults[m_bookedWeights[_idx_weight]];
    if (_toFillValues.size() != _toFillSelections.size() || _toFillValues.size() != _toFillWeights.size() || _toFillWeights.size() != _toFillSelections.size()) MessageSvc::Error("ERROR FILLING EVENT LOOP HAPPENED, FATAL ERROR", "EXIT_FAILURE");

    int    _nBins = m_bookedVariable[_idx_var].NBins();
    double _min   = m_bookedVariable[_idx_var].Min();
    double _max   = m_bookedVariable[_idx_var].Max();

    TString _nameVar    = m_bookedVariable[_idx_var].Expression();
    _nameVar = ((TString) _nameVar).ReplaceAll("(", "_").ReplaceAll("TMath::", "").ReplaceAll(")", "").ReplaceAll("{", "").ReplaceAll("}", "");
    TString _nameCut    = m_bookedSelections[_idx_cut].Label();
    TString _nameWeight = m_bookedWeights[_idx_weight].Label();
    TString _nameHisto  = _nameVar + " [ " +_nameCut + "," + _nameWeight + " ]";
    TString _titleHisto = _nameVar + " [ " +_nameCut +" , "+ _nameWeight + " ] ";
    if( _option.Contains("noVar")){
    	_titleHisto.ReplaceAll(_nameVar, "").ReplaceAll(" [ ", "").ReplaceAll(" ] ",""); 
    }
    if( _option.Contains("noCut")){
    	_titleHisto.ReplaceAll(_nameCut, "").ReplaceAll(",", "");
    }
    if( _option.Contains("noWeight")){
    	_titleHisto.ReplaceAll(_nameWeight, "").ReplaceAll(",", "");    	
    }

    int _nUnderflow = 0;
    int _nOverFlow  = 0;

    // cout<<RED<< fmt::format("{0}, {1}, bins {2}, min {3}, max{4} ", _nameHisto, _titleHisto, _nBins, _min, _max) << endl;
    TH1D * _hret = new TH1D(_nameHisto, _titleHisto, m_bookedVariable[_idx_var].NBins(), _min, _max);

    for (int i = 0; i < _toFillValues.size(); ++i) {
        const double _value  = _toFillValues[i];
        const bool   _sel    = _toFillSelections[i];
        const double _weight = _toFillWeights[i];
        if (!_sel) continue;
        if (_value < _min) {
            _nUnderflow++;
            continue;
        }
        if (_value > _max) {
            _nOverFlow++;
            continue;
        }
        _hret->Fill(_value, _weight);
    }

    if (_nUnderflow > 0) MessageSvc::Debug("GetHistogram1D", (TString) "UnderFlow =", to_string(_nUnderflow), "below", to_string(_min), "for", m_bookedVariable[_idx_var].Expression(), "with Selection", m_bookedSelections[_idx_cut].Label());
    if (_nOverFlow > 0) MessageSvc::Debug("GetHistogram1D", (TString) "OverFlow =", to_string(_nOverFlow), "above", to_string(_max), "for", m_bookedVariable[_idx_var].Expression(), "with Selection", m_bookedSelections[_idx_cut].Label());

    // Re-work axis labels here
    TString _xAxis = m_bookedVariable[_idx_var].Label();
    if (m_bookedVariable[_idx_var].Units() != "") { _xAxis += " [" + m_bookedVariable[_idx_var].Units() + "]"; }
    _hret->GetXaxis()->SetTitle(_xAxis);
    TString _yAxis = Form("Counts/%.2f", abs(_max - _min) / m_bookedVariable[_idx_var].NBins());
    if (m_bookedVariable[_idx_var].Units() != "") { _yAxis += " [" + m_bookedVariable[_idx_var].Units() + "]"; }
    _hret->GetYaxis()->SetTitle(_yAxis);

    _hret->SetLineColor(_color);
    _hret->SetMarkerColor(_color);
    return _hret;
}

TH2D * RXDataPlayer::GetHistogram2D(std::pair< int, int > _idx_var, int _idx_cut, int _idx_weight) {
    int _idx_varX = _idx_var.first;
    int _idx_varY = _idx_var.second;

    if (_idx_varX   >= GetNVariables() ) MessageSvc::Error("RXDataPlayer", "GetHistogram2D", to_string(_idx_varX), to_string(GetNVariables()), "not existing Variable", "EXIT_FAILURE");
    if (_idx_varY   >= GetNVariables() ) MessageSvc::Error("RXDataPlayer", "GetHistogram2D", to_string(_idx_varY), to_string(GetNVariables()), "not existing Variable", "EXIT_FAILURE");
    if (_idx_cut    >= GetNSelections()) MessageSvc::Error("RXDataPlayer", "GetHistogram2D", to_string(_idx_cut), to_string(GetNSelections()), "not existing Selection", "EXIT_FAILURE");
    if (_idx_weight >= GetNWeights()   ) MessageSvc::Error("RXDataPlayer", "GetHistogram2D", to_string(_idx_weight), to_string(GetNWeights()), "not existing Weight", "EXIT_FAILURE");

    // Retrieve Histogram from Var-IDX_X
    int     _nBinsX   = m_bookedVariable[_idx_varX].NBins();
    double  _minX     = m_bookedVariable[_idx_varX].Min();
    double  _maxX     = m_bookedVariable[_idx_varX].Max();
    TString _nameVarX = m_bookedVariable[_idx_varX].Expression();
    int     _nBinsY   = m_bookedVariable[_idx_varY].NBins();
    double  _minY     = m_bookedVariable[_idx_varY].Min();
    double  _maxY     = m_bookedVariable[_idx_varY].Max();
    TString _nameVarY = m_bookedVariable[_idx_varY].Expression();

    TString _nameCut    = m_bookedSelections[_idx_cut].Label();
    TString _nameWeight = m_bookedWeights[_idx_weight].Label();

    TString _nameVarY_clean = ((TString) _nameVarY).ReplaceAll("(", "_").ReplaceAll("TMath::", "").ReplaceAll(")", "").ReplaceAll("{", "").ReplaceAll("}", "");
    TString _nameVarX_clean = ((TString) _nameVarX).ReplaceAll("(", "_").ReplaceAll("TMath::", "").ReplaceAll(")", "").ReplaceAll("{", "").ReplaceAll("}", "");

    TString _nameHisto  = fmt::format("{0}_{1}_{2}_{3}", _nameVarY_clean, _nameVarX_clean, _nameCut, _nameWeight);
    TString _titleHisto = fmt::format("{0} vs {1} [{2}, {3}]", _nameVarY, _nameVarX, _nameCut, _nameWeight);

    int _nUnderFlowX = 0;
    int _nOverFlowX  = 0;

    int _nUnderFlowY = 0;
    int _nOverFlowY  = 0;

    // cout<<RED<< fmt::format("{0}, {1}, bins {2}, min {3}, max{4} ", _nameHisto, _titleHisto, _nBins, _min, _max) << endl;
    auto             _xVar   = GetVariableValues(_idx_varX);
    auto             _yVar   = GetVariableValues(_idx_varY);
    auto             _sel    = GetSelectionValues(_idx_cut);
    auto             _weight = GetWeightValues(_idx_weight);
    vector< size_t > _sizes  = {_xVar.size(), _yVar.size(), _sel.size(), _weight.size()};

    bool _equal = all_of(_sizes.begin(), _sizes.end(), [&_sizes](size_t _item) { return _item == _sizes[0]; });
    if (!_equal) MessageSvc::Error("Something went wrong, aborting", "EXIT_FAILURE");
    // TH2D TH2D(const char* name, const char* title, Int_t nbinsx, Double_t xlow, Double_t xup, Int_t nbinsy, Double_t ylow, Double_t yup)
    TH2D * _hret = new TH2D(_nameHisto, _titleHisto, _nBinsX, _minX, _maxX, _nBinsY, _minY, _maxY);
    for (auto && _entry : zip_range(_xVar, _yVar, _sel, _weight)) {
        if (!_entry.get< 2 >()) { continue; }
        double _x      = _entry.get< 0 >();
        double _y      = _entry.get< 1 >();
        double _weight = _entry.get< 3 >();
        if (_x < _minX) { _nUnderFlowX++; }
        if (_x > _maxX) { _nOverFlowX++; }
        if (_y < _minY) { _nUnderFlowY++; }
        if (_y > _maxY) { _nOverFlowY++; }
        _hret->Fill(_x, _y, _entry.get< 3 >());
    }

    if (_nUnderFlowX > 0) MessageSvc::Debug("GetHistogram2D", (TString) "UnderFlowX =", to_string(_nUnderFlowX), "below", to_string(_minX), "for", m_bookedVariable[_idx_varX].Expression(), "with Selection", m_bookedSelections[_idx_cut].Label());
    if (_nOverFlowX > 0) MessageSvc::Debug("GetHistogram2D", (TString) "OverFlowX =", to_string(_nOverFlowX), "above", to_string(_maxX), "for", m_bookedVariable[_idx_varX].Expression(), "with Selection", m_bookedSelections[_idx_cut].Label());
    if (_nUnderFlowY > 0) MessageSvc::Debug("GetHistogram2D", (TString) "UnderFlowY =", to_string(_nUnderFlowY), "below", to_string(_minY), "for", m_bookedVariable[_idx_varY].Expression(), "with Selection", m_bookedSelections[_idx_cut].Label());
    if (_nOverFlowY > 0) MessageSvc::Debug("GetHistogram2D", (TString) "OverFlowY =", to_string(_nOverFlowY), "above", to_string(_maxY), "for", m_bookedVariable[_idx_varY].Expression(), "with Selection", m_bookedSelections[_idx_cut].Label());

    // Re-work axis labels here
    TString _xAxis = m_bookedVariable[_idx_varX].Label();
    TString _yAxis = m_bookedVariable[_idx_varY].Label();
    if (m_bookedVariable[_idx_varX].Units() != "") { _xAxis += " [" + m_bookedVariable[_idx_varX].Units() + "]"; }
    if (m_bookedVariable[_idx_varY].Units() != "") { _yAxis += " [" + m_bookedVariable[_idx_varY].Units() + "]"; }
    _hret->GetXaxis()->SetTitle(_xAxis);
    _hret->GetYaxis()->SetTitle(_yAxis);
    TString _zAxis = "Counts";
    _hret->GetZaxis()->SetTitle(_zAxis);
    
    return _hret;
}


vector< double > RXDataPlayer::GetVariableValues(int _idx) {
    if (_idx > GetNVariables()) MessageSvc::Error("Requesting IDX > stored ones", "EXIT_FAILURE");
    return m_bookedVariableValues[m_bookedVariable[_idx]];
}

vector< bool > RXDataPlayer::GetSelectionValues(int _idx) {
    if (_idx > GetNSelections()) MessageSvc::Error("Requesting IDX > stored ones", "EXIT_FAILURE");
    return m_bookedSelectionResults[m_bookedSelections[_idx]];
}

vector< double > RXDataPlayer::GetWeightValues(int _idx) {
    if (_idx > GetNWeights()) MessageSvc::Error("Requesting IDX > stored ones", "EXIT_FAILURE");
    return m_bookedWeightResults[m_bookedWeights[_idx]];
}

#endif
