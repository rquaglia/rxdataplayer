#ifndef RXDATAPLAYER_HPP
#define RXDATAPLAYER_HPP

#include "MessageSvc.hpp"
#include "TString.h"
#include <map>
#include <tuple>
#include "TH2D.h"
#include "TH1D.h"
#include "TChain.h"
#include <utility>

/* By Renato Quagliani
 * (rquaglia@cern.ch)
 * February 2018
 *  [v0 for RX analysis in LHCB software]
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
/**
 * @brief      Class for order in the tuple< Var,Sel,Weight> when wrapping the containers
 */
enum class Order : int { Selection = 0, Weight = 1, Variable = 2 };

/**
 * @brief      Class housing infos for the rx weight processing/plotting
 */
class RXWeight {
  public:
    /**
     * \brief Default constructor
     */
    RXWeight(){};

    /**
     * @brief      Constructs the object with an IDX, an Expression, The Actual Label
     *
     * @param[in]  _idx        The index
     * @param[in]  _expression The weight expression
     * @param[in]  _label      The weight label
     */
    RXWeight(int _idx, TString _expression, TString _label) {
        m_idx       = _idx;
        m_weightExp = _expression;
        m_weightLab = _label;
    };

    /**
     * The WeightExpression to use in TTreeFormula when loading the value
     */
    TString m_weightExp;

    /**
     * The WeightLabel associated to this Weight, it will populate the final Histogram
     */
    TString m_weightLab;

    /**
     * The unique IDX identifying this object, the idx is used such that Weight1 == Weight2 if idx == idx, useful to store map and retrieve object associated to a given objec
     */
    int m_idx;

    /**
     * @brief      Returnm this oject Weight Expression
     *
     * @return     { description_of_the_return_value }
     */
    TString Expression() const { return m_weightExp; };

    /**
     * @brief      Return the label of This Object
     *
     * @return     { description_of_the_return_value }
     */
    TString Label() const { return m_weightLab; };

    /**
     * @brief      Print for Debugging
     */
    void Print() const {
        MessageSvc::Line();
        MessageSvc::Info("Weight", (TString) "ID =",         to_string(Index()));
        MessageSvc::Info("Weight", (TString) "Label =",      m_weightLab);
        MessageSvc::Info("Weight", (TString) "Expression =", m_weightExp);
        MessageSvc::Line();
    };

    /**
     * @brief      The Weeight Index
     *
     * @return    the Weight INdex
     */
    int Index() const { return m_idx; };

    bool operator<(const RXWeight & rhs) const { return m_idx < rhs.Index(); };
    bool operator==(const RXWeight & rhs) { return m_idx == rhs.Index(); };
};

/**
 * @brief      Class housing info for the rx selection to apply when plotting
 */
class RXSelection {
  public:
    /**
     * \brief Default constructor
     */
    RXSelection(){};

    /**
     * @brief      Constructs the object. with IDX, the actual selection and the SelectionLabel
     *
     * @param[in]  _idx        The index
     * @param[in]  _expression The rx selection
     * @param[in]  _label      The rx selection label
     */
    RXSelection(int _idx, TCut _expression, TString _label) {
        m_idx          = _idx;   // idx*MAXSLOTS;
        m_selectionExp = _expression;
        m_selectionLab = _label;
    };

    /**
     * The actual cut to apply
     */
    TCut m_selectionExp;

    /**
     * The label for the cut
     */
    TString m_selectionLab;

    /**
     * The unique IDX identifying this Cut in a given sequence of cuts
     */
    int m_idx;

    /**
     * @brief      Cut Index of this Selection
     *
     * @return     The cut index of the selection
     */
    int Index() const { return m_idx; };

    /**
     * @brief      Print this RXSelection
     */
    void Print() const {
        MessageSvc::Line();
        MessageSvc::Info("Selection", (TString) "ID =",         to_string(Index()));
        MessageSvc::Info("Selection", (TString) "Label =",      m_selectionLab);
        MessageSvc::Info("Selection", (TString) "Expression =", TString(m_selectionExp));
        MessageSvc::Line();
    };

    /**
     * @brief      Expression Getter
     *
     * @return     Get The Expression in form of TString
     */
    TString Expression() const { return (TString) m_selectionExp; };

    /**
     * @brief      Label Getter
     *
     * @return     Get the Label in form of TString
     */
    TString Label() const { return m_selectionLab; };

    /**
     * @brief      Operator equality
     * @return     true if they have the same internal Index
     */
    bool operator==(const RXSelection & a) { return m_idx == a.Index(); };

    /**
     * @brief      Operator needed when storing map[RXSelection, ...]; order in map is given by the < operator, thus the idx sorting
     *
     * @param[in]  rhs  Another RXSelection
     *
     * @return     the idx comparison
     */
    bool operator<(const RXSelection & rhs) const { return m_idx < rhs.Index(); };
};

class RXVarPlot {
  public:
    /**
     * \brief Default constructor
     */
    RXVarPlot(){};

    /**
     * @brief      Constructs the object.
     *
     * @param[in]  _idx         The index to assign
     * @param[in]  _expression  The variable expression
     * @param[in]  _label       The variable label
     * @param[in]  _nBins       The bins
     * @param[in]  _min         The minimum
     * @param[in]  _max         The maximum
     * @param[in]  _units       The units
     * @param[in]  _doIsoBinned The do iso binned [not handled in RXDataPlayer]
     * @param[in]  _useLogY     The use y log [not supported]
     */
    RXVarPlot(int _idx, TString _expression, TString _label, int _nBins, double _min, double _max, TString _units = "", bool _doIsoBinned = false, bool _useLogY = false) {
        m_idx         = _idx;
        m_varExp      = _expression;
        m_varLab      = _label;
        m_nBins       = _nBins;
        m_min         = _min;
        m_max         = _max;
        m_units       = _units;
        m_useLogY     = _useLogY;
        m_doIsobinned = _doIsoBinned;
    };

    TString m_varExp;
    TString m_varLab;
    TString m_units;
    int     m_idx;
    bool    m_useLogY;
    bool    m_doIsobinned;
    double  m_min;
    double  m_max;
    int     m_nBins;

    /**
     * @brief      GetExpression to use in Draw
     *
     * @return     The Expression for Drawing this RXVarPlot
     */
    TString Expression() const { return m_varExp; };

    /**
     * @brief      The Label (used to assign x-axis name)
     *
     * @return     The Label of this Variable
     */
    TString Label() const { return m_varLab; };

    /**
     * @brief      The units in which this observable is looked for (attached to y and x axis)
     *
     * @return     { description_of_the_return_value }
     */
    TString Units() { return m_units; };

    /**
     * @brief      The nBins one wants to use
     *
     * @return     The number of bins
     */
    int NBins() { return m_nBins; };

    /**
     * @brief      Min boundary of this variable
     *
     * @return     the min
     */
    double Min() { return m_min; };

    /**
     * @brief      Max boundary of this variable
     *
     * @return     the max
     */
    double Max() { return m_max; };

    /**
     * @brief      Logs on y-axis for plot?
     *
     * @return     doLogY on canvas?
     */
    bool LogY() { return m_useLogY; };

    /**
     * @brief      IsoBin this observable?
     *
     * @return     True/False
     */
    bool IsoBin() { return m_doIsobinned; };

    /**
     * @brief      The INdex of this RXVarPlot
     *
     * @return     The Index used for bookkeping in RXDataPlayer
     */
    int Index() const { return m_idx; };

    /**
     * @brief      Print this RXVarPlot
     */
    void Print() const {
        MessageSvc::Line();
        MessageSvc::Info("Variable", (TString) "ID =",             to_string(Index()));
        MessageSvc::Info("Variable", (TString) "Label =",          m_varLab, m_units);
        MessageSvc::Info("Variable", (TString) "Expression =",     m_varExp);
        MessageSvc::Info("Variable", (TString) "nBins[min,max] =", to_string(m_nBins), "[", to_string(m_min), ",", to_string(m_max), "]");
        MessageSvc::Info("Variable", (TString) "iso =",            to_string(m_doIsobinned));
        MessageSvc::Info("Variable", (TString) "logY =",           to_string(m_useLogY));
        MessageSvc::Line();
    };

    /**
     * @brief      Oparator equality
     *
     * @param[in]  a     one RXVarPlot
     * @param[in]  b     one RXVarPlot
     *
     * @return     equal if they share the smae index
     */
    bool operator==(const RXVarPlot & a) { return m_idx == a.Index(); };

    /**
     * @brief      Operator <, useful for map<RXVarPlot,...>
     *
     * @param[in]  rhs   Another RXVarPlot to compare with
     *
     * @return     Index comparison
     */
    bool operator<(const RXVarPlot & rhs) const { return m_idx < rhs.Index(); };
};

/**
 * @brief      The RXDataPlayer handling and bookkeping histograms-cuts-selections to look at for a given input TChain
 */
class RXDataPlayer {
  public:
    /**
     * \brief Default constructor
     */
    RXDataPlayer();

    /**
     * \briefConstructor with TString
     */
    RXDataPlayer(TString _name);

    /**
     * @brief
     *
     * @param[in]  _expression  The variable expression to use in Drawing
     * @param[in]  _label       The variable label to attach to the X-Axis
     * @param[in]  _nBins       The bins on which to plot
     * @param[in]  _min         The minimum on which to plot
     * @param[in]  _max         The maximum on which to plot
     * @param[in]  _units       The units to use on this variable
     * @param[in]  _isoBin      The iso bin flag [not used yet]
     * @param[in]  _useLogY     The use logy [not used yet, no "full-plot report available at the moment"]
     */
    void Book1DHist(TString _expression, TString _label, int _nBins, double _min, double _max, TString _units = "", bool _isoBin = false, bool _useLogY = false);

    /**
     * @brief      Book1D histo passing a RooRealVar, we will use the name, title, nBins, units to book the histogram
     *
     * @param[in]  var   The variable
     */
    void Book1DHist(const RooRealVar & var);

    // void Book1DHist(TString _expression, int nBins, double min, double max  ,TString units, bool isoBin = false, bool _useLogY = false);
    /**
     * @brief      Add a Selection Switcher for all the variables in the pool
     *
     * @param[in]  _selection  The selection expression (TCut)
     * @param[in]  _label      The label of the selection
     */
    void BookSelection(TCut _selection, TString _label);

    /**
     * @brief      BookWeight
     *
     * @param[in]  _weight The weight
     * @param[in]  _label  The rx weight label
     */
    void BookWeight(TString _weight, TString _label);

    /**
     * @brief      Gets the n selections one has booked internally
     *
     * @return     The n selections.
     */
    int GetNSelections() const { return m_nSelections; };

    /**
     * @brief      Gets the n weight one has booked internally.
     *
     * @return     The n weight exponent.
     */
    int GetNWeights() const { return m_nWeights; };

    /**
     * @brief      Gets the n variable plot booked internally.
     *
     * @return     The n variables to plot.
     */
    int GetNVariables() const { return m_nVars; };

    /**
     * @brief      Gets the std::vector<values> of the Observable added at the INDEX = _idx
     *
     * @param[in]  _idx  The index variable to retrieve the std::vector<double>, having size = nEntries Tuple processed
     *
     * @return     The std::vector<double> sorted by "entry" processing order.
     */
    std::vector< double > GetVariableValues(int _idx);

    /**
     * @brief      Gets the std::vector<bool> associated to the booked selection with INDEX = _idx.
     *
     * @param[in]  _idx  The index of the selection to retrieve
     *
     * @return     The std::vector<bool> sorted by "entry" processing order.
     */
    std::vector< bool > GetSelectionValues(int _idx);

    /**
     * @brief      Gets the std::vector<double> associated to the booked weight with INDEX = _idx.
     *
     * @param[in]  _idx  The index weight
     *
     * @return     The std::vector<double> sorted by "entry" processing order.
     */
    std::vector< double > GetWeightValues(int _idx);

    /**
     * @brief      Print the Booked Stuff
     */
    void Print() {
        MessageSvc::Line();
        MessageSvc::Info("RXDataPlayer", m_name);
        MessageSvc::Line();
        MessageSvc::Info("Booked Variable(s)", to_string(GetNVariables()));
        for (const auto & vv : m_bookedVariable) { vv.second.Print(); }
        MessageSvc::Info("Booked Selection(s)", to_string(GetNSelections()));
        for (const auto & vv : m_bookedSelections) { vv.second.Print(); }
        MessageSvc::Info("Booked Weight(s)", to_string(GetNWeights()));
        for (const auto & vv : m_bookedWeights) { vv.second.Print(); }
        MessageSvc::Line();
    };

    /**
     * @brief      Builds the cross Product of combinations, Selection, Weights : size nSel x nObs x nWeights
     *
     * @return     The combination.
     */
    std::vector< tuple< RXSelection, RXWeight, RXVarPlot > > BuildCombination();

    /**
     * @brief      Process This
     *
     * @param      _tuple  The input TChain to process
     */
    void Process(TChain & _tuple) noexcept;

    /**
     * @brief      Build and return an Histogram with the Booked properties of RXSelection bookkeped for the Variable(_idx_var), with Cut(_idx_cut) and Weight(_idx_weight)
     *
     * @param[in]  _idx_var     The index variable
     * @param[in]  _idx_cut     The index cut
     * @param[in]  _idx_weight  The index weight
     * @param[in]  _coplor      The color index to use (kBlack, kRed, kGreen ) are integer numbers. 
     * @param[in]  _option      The option for "auto-titles" , 
     * 													by default the title is VarLabel [CutLabel,WeightLabel] , 
     * 													Option = "noVar" -> drop VariableLable, 
     * 																	 "noCut" , drop Cut, 
     * 																	 "noWeight" , dropWeight
     * 																	 
     * @return     The histogram.
     */
    TH1D * GetHistogram1D(int _idx_var, int _idx_cut, int _idx_weight, Int_t _color = 1 , TString _option = "");

    /**
     * @brief      Gets the histogram 2D
     *
     * @param[in]  _idx_varX    The index of  variable for x-axis
     * @param[in]  _idx_varY    The index of  variable for y-axis
     * @param[in]  _idx_cut     The index of the selection to apply cut
     * @param[in]  _idx_weight  The index of the weight to apply
     *
     * @return     The histogram built with the specifics (bins wrapped from the "binner"); TODO : optional nBinsX, nBinsY to overload the current values
     */
    TH2D * GetHistogram2D(std::pair< int, int > _idx_var, int _idx_cut, int _idx_weight);

    /**
     * @brief      Clean-Up all maps, restore to default, can re-use same object.
     */
    void Reset() {
        m_bookedWeights.clear();
        m_bookedSelections.clear();
        m_bookedVariable.clear();
        m_bookedWeightResults.clear();
        m_bookedSelectionResults.clear();
        m_bookedVariableValues.clear();
        m_nSelections = 0;   // 0-99
        m_nWeights    = 0;   // 0-99
        m_nVars       = 0;   // 0-99
        return;
    };

    /**
     * @brief      Clean-up the stored from processed TChain, one can ClearContent and re-fill from TChain.
     */
    void ClearContent() {
        m_bookedWeightResults.clear();
        m_bookedSelectionResults.clear();
        m_bookedVariableValues.clear();
    };

    // void CutFlow( const std::vector<int> & chain_selection_idxs);


  private:
    map< int, RXWeight >    m_bookedWeights;
    map< int, RXSelection > m_bookedSelections;
    map< int, RXVarPlot >   m_bookedVariable;

    map< RXWeight, std::vector< double > >  m_bookedWeightResults;
    map< RXSelection, std::vector< bool > > m_bookedSelectionResults;
    map< RXVarPlot, std::vector< double > > m_bookedVariableValues;

    int m_nSelections = 0;
    int m_nWeights    = 0;
    int m_nVars       = 0;

    TString m_name = "RXDataPlayer";

    bool m_debug = false;
    /**
     * \brief Activate debug
     * @param _debug [description]
     */
    void SetDebug(bool _debug) { m_debug = _debug; };
};

#endif
