#ifndef MESSAGESVC_HPP
#define MESSAGESVC_HPP

#include <assert.h>
#include <iostream>
#include <iomanip>
#include <stdexcept>
#include <typeinfo>

#include "TCut.h"
#include "TH1.h"
#include "TH2.h"
#include "TString.h"

#include "RooAbsPdf.h"
#include "RooAbsReal.h"
#include "RooDataSet.h"
#include "RooFormulaVar.h"
#include "RooRealVar.h"

using namespace std;

#define RESET   "\033[0m"
#define BLACK   "\033[30m"
#define RED     "\033[31m"
#define GREEN   "\033[32m"
#define YELLOW  "\033[33m"
#define BLUE    "\033[34m"
#define MAGENTA "\033[35m"
#define CYAN    "\033[36m"
#define WHITE   "\033[37m"

/**
 * \class MessageSvc
 * \brief Message service
 */
class MessageSvc {

public :
    /**
     * \brief Constructor
     */
    MessageSvc() = default;

    static void Debug(TString _func, TString _string1 = "", TString _string2 = "", TString _string3 = "", TString _string4 = "", TString _string5 = "", TString _string6 = "", TString _string7 = "", TString _string8 = "") {
        cout << YELLOW;
        Print(cout, Message("DEBUG"), _func, _string1, _string2, _string3, _string4, _string5, _string6, _string7, _string8);
        cout << RESET;
    };
    template <typename T>
    static void Debug(TString _func, T * _t, TString _option = "") {
        cout << YELLOW;
        cout << setw(m_setw1) << left << Message("DEBUG");
        cout << setw(m_setw2) << left << _func;
        if (_t != nullptr) {
            _t->Print("");
            if (_option == "v") _t->Print("v");
        }
        else Error(_func, (TString) "Object is nullptr", "EXIT_FAILURE");
        cout << RESET;
    };

    static void Error(TString _func, TString _string1 = "", TString _string2 = "", TString _string3 = "", TString _string4 = "", TString _string5 = "", TString _string6 = "", TString _string7 = "", TString _string8 = "") {
        cerr << RED;
        cerr << endl;
        Print(cerr, Message("ERROR"), _func, _string1, _string2, _string3, _string4, _string5, _string6, _string7, _string8);
        cerr << RESET;
        if ((_string1 == "logic_error") || (_string2 == "logic_error") || (_string3 == "logic_error") || (_string4 == "logic_error") || (_string5 == "logic_error") || (_string6 == "logic_error")) { throw logic_error(_func); }
        if ((_string1 == "EXIT_FAILURE") || (_string2 == "EXIT_FAILURE") || (_string3 == "EXIT_FAILURE") || (_string4 == "EXIT_FAILURE") || (_string5 == "EXIT_FAILURE") || (_string6 == "EXIT_FAILURE")) exit(EXIT_FAILURE);
    };
    static void Error(int _expression, TString _func, TString _string1 = "", TString _string2 = "", TString _string3 = "", TString _string4 = "", TString _string5 = "", TString _string6 = "", TString _string7 = "", TString _string8 = "") {
        if (_expression) {
            Error(_func, _string1, _string2, _string3, _string4, _string5, _string6, _string7, _string8);
            if ((_string1 == "assert") || (_string2 == "assert") || (_string3 == "assert") || (_string4 == "assert") || (_string5 == "assert") || (_string6 == "assert")) assert(_expression);
        }
    };

    static void Info(TString _func, TString _string1 = "", TString _string2 = "", TString _string3 = "", TString _string4 = "", TString _string5 = "", TString _string6 = "", TString _string7 = "", TString _string8 = "") {
        cout << GREEN;
        Print(cout, Message("INFO"), _func, _string1, _string2, _string3, _string4, _string5, _string6, _string7, _string8);
        cout << RESET;
    };
    static void Info(TString _func, bool _property) {
        cout << GREEN;
        Print(cout, Message("INFO"), _func, TString(_property ? "True" : "False"));
        cout << RESET;
    };
    template <typename T>
    static void Info(TString _func, T * _t, TString _option = "") {
        cout << GREEN;
        cout << setw(m_setw1) << left << Message("INFO");
        cout << setw(m_setw2) << left << _func;
        if (_t != nullptr) {
            _t->Print("");
            if (_option == "v") _t->Print("v");
            if (((TString) typeid(_t).name()).Contains("RooRealVar"))
                Print(cout, " ", "RooRealVar",
                      to_string(((RooRealVar *) _t)->getVal()),     "+/-",
                      to_string(((RooRealVar *) _t)->getError()),   "( + " + to_string(((RooRealVar *) _t)->getErrorHi()) + ", - " + to_string(((RooRealVar *) _t)->getErrorLo()) + " )",
                      ((RooRealVar *) _t)->getError() != 0 ? " (" + to_string(((RooRealVar *) _t)->getError() / ((RooRealVar *) _t)->getVal() * 100) + "%)" : "");
            if (((TString) typeid(_t).name()).Contains("TH1")) {
                Print(cout, " ", "Entries",    to_string(((TH1 *) _t)->GetEntries()));
                Print(cout, " ", "Integral",   to_string(((TH1 *) _t)->Integral()));
                Print(cout, " ", "Underflows", to_string(((TH1 *) _t)->GetBinContent(0)));
                Print(cout, " ", "Overflows",  to_string(((TH1 *) _t)->GetBinContent(((TH1 *) _t)->GetNbinsX() + 1)));
                Print(cout, " ", "Mean",       to_string(((TH1 *) _t)->GetMean()),   "+/-", to_string(((TH1 *) _t)->GetMeanError()));
                Print(cout, " ", "StdDev",     to_string(((TH1 *) _t)->GetStdDev()), "+/-", to_string(((TH1 *) _t)->GetStdDevError()));
            }
            if (((TString) typeid(_t).name()).Contains("TH2")) {
                Print(cout, " ", "Entries",    to_string(((TH2 *) _t)->GetEntries()));
                Print(cout, " ", "Integral",   to_string(((TH2 *) _t)->Integral()));
            }
        }
        else Error(_func, (TString) "Object is nullptr", "EXIT_FAILURE");
        cout << RESET;
    };
    template <typename T>
    static void Info(TString _func, vector <T> _ts) {
        cout << GREEN;
        cout << setw(m_setw1) << left << Message("INFO");
        cout << setw(m_setw2) << left << _func;
        cout << "[" << _ts.size() << "] ";
        if (((TString) typeid(_ts).name()).Contains("TCut")) cout << endl;
        for (const auto & _t : _ts) {
            if (((TString) typeid(_t).name()).Contains("TCut"))         cout << setw(m_setw1 + m_setw2) << left << " " << _t << endl;
            else if (((TString) typeid(_t).name()).Contains("TString")) cout << "\"" << _t << "\" ";
            else                                                        cout << _t << " ";
        }
        if (! ((TString) typeid(_ts).name()).Contains("TCut")) cout << endl;
        cout << RESET;
    };

    static void Warning(TString _func, TString _string1 = "", TString _string2 = "", TString _string3 = "", TString _string4 = "", TString _string5 = "", TString _string6 = "", TString _string7 = "", TString _string8 = "") {
        cout << MAGENTA;
        Print(cout, Message("WARNING"), _func, _string1, _string2, _string3, _string4, _string5, _string6, _string7, _string8);
        cout << RESET;
    };
    template <typename T>
    static void Warning(TString _func, T * _t, TString _option = "") {
        cout << MAGENTA;
        cout << setw(m_setw1) << left << Message("WARNING");
        cout << setw(m_setw2) << left << _func;
        if (_t != nullptr) {
            _t->Print("");
            if (_option == "v") _t->Print("v");
        }
        else Error(_func, (TString) "Object is nullptr", "EXIT_FAILURE");
        cout << RESET;
    };

    static void Print(TString _string1, TString _string2 = "", TString _string3 = "", TString _string4 = "", TString _string5 = "", TString _string6 = "", TString _string7 = "", TString _string8 = "", TString _string9 = "") {
        Print(cout, _string1, _string2, _string3, _string4, _string5, _string6, _string7, _string8, _string9);
    };
    static void Print(ostream & os, TString _string1, TString _string2 = "", TString _string3 = "", TString _string4 = "", TString _string5 = "", TString _string6 = "", TString _string7 = "", TString _string8 = "", TString _string9 = "", TString _string10 = "") {
        os << setw(m_setw1) << left << _string1;
        os << setw(m_setw2) << left << _string2;
        if (_string3  != "") os << _string3  << " ";
        if (_string4  != "") os << _string4  << " ";
        if (_string5  != "") os << _string5  << " ";
        if (_string6  != "") os << _string6  << " ";
        if (_string7  != "") os << _string7  << " ";
        if (_string8  != "") os << _string8  << " ";
        if (_string9  != "") os << _string9  << " ";
        if (_string10 != "") os << _string10 << " ";
        os << endl;
    };

    static void Line() { Line(cout); };
    static void Line(ostream & os) { os << string(m_line, '=') << endl; };

    static TString Message(TString _message) { return string(m_char, '=') + " " + _message + " " + string(m_char, '=') + " "; };

    static double ShowPercentage(int ientry, int nentries, time_t start = 0, int ntimes = 2000, bool dobar = true, bool doentry = true) {
        static int first_entry = ientry;

        static int div = (nentries - first_entry) / ntimes;
        if (div < 1) div = 1;

        int myentry = ientry - first_entry;
        if ((((int) (myentry) % div) == 0) || (myentry == 0)) {
            //static int i = 0;
            //if(i > 3) i = 0;
            //char c[] = {'|','/','-','\\'};
            //cout << "\r" << c[++i];
            double perc = (double) ientry / (nentries - 1);

            cout << GREEN;
            cout << "\r";
            cout << setw(m_setw1) << left << Message("INFO");
            cout << setw(m_setw2) << left << "ShowPercentage";
            cout << fixed << setprecision(1) << perc * 100 << "%  ";

            if (dobar) {
                cout << WHITE << "[";
                for (int p = 0; p < perc * 20; p++)       cout << ">";
                for (int p = 0; p < (1 - perc) * 20; p++) cout << "_";
                cout << "]" << "  ";
                cout << GREEN;
            }

            if (doentry) cout << "Entry # " << ientry + 1;

            if (start != 0) {
                time_t stop = time(nullptr);
                double diff = difftime(stop, start);
                cout << "  (";

                double t_left = ((double) nentries / ientry - 1) * diff;

                cout << "~";
                if (t_left > 60) cout << t_left / 60 << " min to the end)";
                else             cout << t_left << " s to the end)";
                cout << flush;
            }

            if (ientry == (nentries - 1)) cout << RESET << endl;
            return perc;
        }
        if (ientry == (nentries - 1)) cout << RESET << endl;
        return 100;
    };

    static void Banner() {
        cout << YELLOW;
        cout << endl;
        Line();
        Line();
        cout << "||        W     W EEEE L     CCC  OOO  M   M EEEE    TTTTTT  OOO     TTTTTT H  H EEEE    RRRR  X   X    FFFF RRRR   AA  M   M EEEE W     W  OOO  RRRR  K  K      S      ||" << endl;
        cout << "||        W     W E    L    C    O   O MM MM E         TT   O   O      TT   H  H E       R   R  X X     F    R   R A  A MM MM E    W     W O   O R   R K K       B      ||" << endl;
        cout << "||        W  W  W EEE  L    C    O   O M M M EEE       TT   O   O      TT   HHHH EEE     RRRR    X      FFF  RRRR  AAAA M M M EEE  W  W  W O   O RRRR  KK               ||" << endl;
        cout << "||         W W W  E    L    C    O   O M   M E         TT   O   O      TT   H  H E       R R    X X     F    R R   A  A M   M E     W W W  O   O R R   K K       R      ||" << endl;
        cout << "||          W W   EEEE LLLL  CCC  OOO  M   M EEEE      TT    OOO       TT   H  H EEEE    R  RR X   X    F    R  RR A  A M   M EEEE   W W    OOO  R  RR K  K      Q      ||" << endl;
        Line();
        Line();
        cout << endl;
        cout << RESET;
    };

private :
    static const int m_line = 170;
    static const int m_char = 1;

    static const int m_setw1 = 20;
    static const int m_setw2 = 35;
};

#endif
